//Teste Inicial

cadastrados = getObjectLocalStorage("cadastrados");
if (cadastrados == null) {
    cadastrados = [];
    setObjectLocalStorage("cadastrados", cadastrados);
}

//Funções

function cadastro() {
    if (typeof (Storage) !== "undefined") {
        var nome = document.getElementById("nomec").value;
        var email = document.getElementById("emailc").value;
        var senha = document.getElementById("senhac").value;

        var usuario = new Usuario(nome, email, senha);

        cadastrados = getObjectLocalStorage("cadastrados");
        if (validaInsert(email)){
            cadastrados.push(usuario);
            setObjectLocalStorage("cadastrados", cadastrados);
        } else{
            window.alert("Já existe um usuário cadastrado com esse e-mail!")
        }
        
        document.getElementById("nomec").value = "";
        document.getElementById("emailc").value = "";
        document.getElementById("senhac").value = "";

    } else {
        window.alert("API Web Storage não encontrada");
    }
}


function login() {
    var email = document.getElementById("email").value;
    var senha = document.getElementById("senha").value;

    var cadastrados = getObjectLocalStorage("cadastrados");

    for (var i=0; i<cadastrados.length; i++){
        var usuario = cadastrados[i];
        if (usuario.email==email && usuario.senha==senha){
            setObjectLocalStorage("logado", usuario);
            window.alert("Bem vindo(a) "+usuario.nome);
            return true
        }
    }
    window.alert("Os dados informados não correspondem a nenhum usuário cadastrado.")
    return false;
}

function logout(){
    localStorage.removeItem("logado");
}

function remove(){
    var cadastrados = getObjectLocalStorage("cadastrados");
    var usuario = getObjectLocalStorage("logado");
    var sure = confirm("Você está prestes a excluir essa conta.\nDeseja continuar?");
    if (sure){
        for (var i = 0; i < cadastrados.length; i++){
            var user = cadastrados[i];
            if (user.email == usuario){
                cadastrados.splice(i, 1);
                setObjectLocalStorage("cadastrados", cadastrados);
                localStorage.removeItem("logado");
                return true;
            }
        }
        window.alert("Usuário logado não consta como cadastrado!!!")
        return false;
    }
}

//Classes

function Usuario(nome, email, senha){
    this.nome = nome;
    this.email = email;
    this.senha = senha;
    this.descricao = function(){
        return "O usuário é: "+this.nome+"!";
    }
}

//Funções Auxiliares

function setObjectLocalStorage(key,value){
	localStorage.setItem(key, JSON.stringify(value));
}

function getObjectLocalStorage(key){
	var value = localStorage.getItem(key);
    return value && JSON.parse(value);
}

function validaInsert(email){
    var cadastrados = getObjectLocalStorage("cadastrados");
    for (var i = 0; i<cadastrados.length; i++){
        var usuario = cadastrados[i];
        if (usuario.email == email){
            return false;
        }
    }
    return true;
}